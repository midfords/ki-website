# Ki Website

This repository contains the code for the Ki product website found [here](https://ki.seanmidford.ca "Title").

It is a simple static website written in HTML, CSS and JS. Resources and icons are included with the source code. Icons are from the Material Design Icons collection.